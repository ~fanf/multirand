#include <stdint.h>

#include "multirand.h"

/*
 * Rejection sampling with one division per rng call. We use `x - r`
 * to round down to a multiple of the `limit`; if this is within
 * `limit` of the maximum, our random number was above the biggest
 * multiple of the `limit`, so we need to reject and resample.
 */
uint32_t
oneper(pcg32_t *rng, uint32_t limit) {
	uint32_t x, r;
	do {
		x = pcg32(rng);
		r = x % limit;
	} while(unlikely(x - r > (uint32_t)-limit));
	return(r);
}
