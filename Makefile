CFLAGS = -O3 -Wall -Wextra

multirand: multirand.o pcg32.o nearly.o really.o oneper.o arc4ru.o slower.o

multirand.o: multirand.c multirand.h bench.c
pcg32.o: pcg32.c multirand.h

nearly.o: nearly.c multirand.h
really.o: really.c multirand.h
oneper.o: oneper.c multirand.h
arc4ru.o: arc4ru.c multirand.h
slower.o: slower.c multirand.h
