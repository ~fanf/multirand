#include <stdint.h>

#include "multirand.h"

uint32_t
slower(pcg32_t *rng, uint32_t limit, uint64_t num) {
	uint32_t residue = (uint32_t)(-limit) % limit;
	while(unlikely((uint32_t)(num) < residue))
		num = (uint64_t)pcg32_fast(rng) * (uint64_t)limit;
        return((uint32_t)(num >> 32));
}
