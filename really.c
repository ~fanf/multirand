#include <stdint.h>

#include "multirand.h"

/*
 * Divisionless unbiased bounded random numbers, based on:
 *
 * Steve Canon - https://github.com/apple/swift/pull/39143
 *
 * Kendall Willets - https://github.com/KWillets/range_generator
 *
 * We calculate `next() * limit` as a 32.32 bit fixed-point number.
 * To debias the integer part (upper 32 bits), we extend the fraction
 * part (lower 32 bits) with unbounded precision. We get more and more
 * random bits until we are sure that carries do (or do not) propagate
 * up to the integer part and change our preliminary value (or not).
 *
 * The debiasing loop guard checks whether we might need to propagate
 * carries by checking for overflow using this trick:
 *
 *	a + b > (1 << 32) - 1
 *	a + b > (uint32_t)-1
 *	a > -b - 1
 *	a > ~b
 *
 * All the carry propagation checks test if something added to the fraction
 * part would overflow, so we keep the fraction part inverted all the time.
 */
uint32_t
really(pcg32_t *rng, uint32_t limit) {
        uint64_t num = (uint64_t)pcg32(rng) * (uint64_t)limit;
	uint32_t value = (uint32_t)(num >> 32);
	uint32_t frac = ~(uint32_t)(num);
	/*
	 * Carries might propagate when limit + frac > (1 << 32) - 1
	 */
        while(unlikely(limit > frac)) {
		num = (uint64_t)pcg32(rng) * (uint64_t)limit;
		uint32_t more = (uint32_t)(num >> 32);
		/*
		 * Carries will propagate when more + frac > (1 << 32) - 1
		 * Carries never propagate when more + frac < (1 << 32) - 1
		 */
		if(likely(more != frac))
			return(value + (more > frac));
		/*
		 * This is the one-in-four-billion case when `more + frac` is
		 * all ones, and we need even more bits to determine the
		 * result. We can throw the all-ones word away because it just
		 * propagates the carry that we might calculate next time: we
		 * do not need bignums to carry with unbounded precision.
		 */
		frac = ~(uint32_t)(num);
	}
        return(value);
}
