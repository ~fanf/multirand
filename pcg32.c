#include <err.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>

#include "multirand.h"

pcg32_t
pcg32_init(void) {
	pcg32_t rng;
        int fd = open("/dev/urandom", O_RDONLY);
        if(fd < 0) err(1, "open /dev/urandom");
        ssize_t n = read(fd, &rng, sizeof(rng));
        if(n < (ssize_t)sizeof(rng)) err(1, "read /dev/urandom");
        close(fd);
	/* the LCG increment must be odd */
	rng.inc |= 1;
	rng.calls = 0;
	return(rng);
}

uint32_t
pcg32(pcg32_t *rng) {
	return(pcg32_fast(rng));
}
