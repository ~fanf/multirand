#include <sys/time.h>

#include <stdint.h>
#include <stdio.h>

#include <err.h>

#include "multirand.h"

static double
doubletime(void) {
	struct timeval tv;
	if(gettimeofday(&tv, NULL) < 0)
		err(1, "gettimeofday");
	return((double)tv.tv_sec + (double)tv.tv_usec / 1000000.0);
}

#define BOUNDED quickr
#include "bench.c"

#define BOUNDED nearly
#include "bench.c"

#define BOUNDED really
#include "bench.c"

#define BOUNDED oneper
#include "bench.c"

#define BOUNDED arc4ru
#include "bench.c"

int
main(void) {
	pcg32_t rng = pcg32_init();

	uint32_t repeat = 100000000;

	printf("%10s %9s %9s %9s %9s %9s %6s %6s %6s %6s %6s\n", "limit",
	       "inline", "nearly", "really", "oneper", "arc4ru",
	       "inline", "nearly", "really", "oneper", "arc4ru");

	uint32_t step = 3;
	for(uint64_t limit = step; limit < UINT32_MAX; limit *= step) {
		s_bench result[] = {
			bench_quickr(&rng, (uint32_t)limit, repeat),
			bench_nearly(&rng, (uint32_t)limit, repeat),
			bench_really(&rng, (uint32_t)limit, repeat),
			bench_oneper(&rng, (uint32_t)limit, repeat),
			bench_arc4ru(&rng, (uint32_t)limit, repeat),
		};
		int N = sizeof(result) / sizeof(s_bench);

		double keepme = 0.0;
		for(int i = 0; i < N; i++)
			keepme += result[i].value;
		int min = 0;
		for(int i = 1; i < N; i++)
			if(result[min].runtime > result[i].runtime)
				min = i;
		int two = !min;
		for(int i = 1; i < N; i++)
			if(result[min].runtime < result[i].runtime &&
			   result[two].runtime > result[i].runtime)
				two = i;

		const char *space[N+1];
		for(int i = 0; i < N+1; i++)
			space[i] = " ";
		space[min+1] = "\033[0m ";
		space[two+1] = "\033[0m ";
		space[min] = " \033[0;1m";
		space[two] = " \033[0;31m";

		printf("%10u",(unsigned)limit);
		for(int i = 0; i < N; i++)
			printf("%10u", (unsigned)result[i].calls);
		for(int i = 0; i < N; i++)
			printf("%s%.4f", space[i], result[i].runtime);
		printf("%s(%+.2f)\n", space[N], keepme);
	}
	return(0);
}
