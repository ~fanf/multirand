#include <stdint.h>

#include "multirand.h"

/*
 * arc4random_uniform()
 */
uint32_t
arc4ru(pcg32_t *rng, uint32_t upper_bound) {
	uint32_t r, min;

	if (upper_bound < 2)
		return 0;

	/* 2**32 % x == (2**32 - x) % x */
	min = -upper_bound % upper_bound;

	/*
	 * This could theoretically loop forever but each retry has
	 * p > 0.5 (worst case, usually far better) of selecting a
	 * number inside the range we need, so it should rarely need
	 * to re-roll.
	 */
	for (;;) {
		r = pcg32(rng);
		if (r >= min)
			break;
	}

	return r % upper_bound;
}
